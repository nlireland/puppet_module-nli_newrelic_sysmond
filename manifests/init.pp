 class newrelic_sysmond (
  $package_source        = $newrelic_sysmond::params::package_source,
  $newrelic_license_key  = $newrelic_sysmond::params::newrelic_license_key,
) inherits newrelic_sysmond::params {

  if $newrelic_license_key == undef {
    fail('The newrelic_license_key parameter must be defined.')
  }

  package {'newrelic-repo':
    provider        => rpm,
    source          => "$package_source",
    install_options => '-Uvh',
    ensure          => installed,
  }

  package {'newrelic-sysmond':
    ensure  => installed,
    require => Package['newrelic-repo'],
  }

  augeas {'newrelic-license-key':
   lens    => "Properties.lns",
   incl    => "/etc/newrelic/nrsysmond.cfg",
   changes => "set license_key $newrelic_license_key",
   require => Package['newrelic-sysmond'],
   notify  => Service['newrelic-sysmond'],
  }

  service {'newrelic-sysmond':
   ensure  => running,
   enable  => true,
   require => Augeas['newrelic-license-key'],
  }
}
