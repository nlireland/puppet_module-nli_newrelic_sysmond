# README #

A puppet module to install the new relic server monitor (newrelic-sysmond) on RHEL/CENTOS.

## Usage ##


```
#!puppet

node 'mynode' {
  class { 'newrelic_sysmond':
    newrelic_license_key => 'YOUR_LICENSE_KEY',
  }
}
```

Or, if your license key is set in Hiera:


```
#!puppet

node 'mynode' {
  include newrelic_sysmond
}
```
